#!/usr/bin/python3

from flask import Flask, request, redirect, url_for, flash, render_template
from werkzeug.utils import secure_filename
from datetime import datetime
import os.path
import pathlib

app = Flask(__name__)

upload_folder = pathlib.Path("uploads")
if not upload_folder.exists():
    upload_folder.mkdir(mode=0o0700)

app.config['UPLOAD_FOLDER'] = upload_folder.as_posix()
app.secret_key = "apmfpawfmapfmapsmawmapfm"


@app.route("/")
def index():
    uploaded_files = []
    for file in upload_folder.iterdir():
        ts = file.stat().st_ctime
        dt = datetime.fromtimestamp(ts)
        uploaded_files.append((file.name, file.read_bytes()[:32], dt.strftime("%Y-%m-%d %H:%M.%S")))
    return render_template("index.html", files=uploaded_files)


@app.route("/upload", methods=["GET", "POST"])
def upload():
    if request.method == "GET":
        return redirect(url_for("index"))

    if "file" not in request.files:
        flash("Error: 'file' not detected in form")
        return redirect(url_for('index'))

    file = request.files["file"]

    if file.filename == '':
        flash('No selected file')
        return redirect(url_for("index"))

    if file:
        print(file)
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return redirect(url_for('index'))

if __name__ == "__main__":
    app.run(debug=True)
