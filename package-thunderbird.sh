#!/bin/bash

full_path=$(realpath $0)
PROJ=$(dirname $full_path)

cd $PROJ/thunderbird
TARGET_ZIP=../acus-thunderbird-$(git rev-parse --short release).zip

if [ -f "$TARGET_ZIP" ]; then
	echo "Deleting existing ZIP file"
	rm $TARGET_ZIP
fi

zip -9 -rv $TARGET_ZIP ./
