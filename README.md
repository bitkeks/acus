# acus

Encryption in Firefox and Thunderbird - powered by WebAssembly!

## Build the wasm binaries

Requires Go (golang) to build, optionally podman/docker.

Switch into ./wasm-wrapper and run `./build.sh`. The script builds the binaries 
and copies them into the addon folders

For reproducible builds use the `build-podman.sh` or `build-docker.sh` scripts, 
depending on your tool of choice. They too build the wasm binaries and put them into the addon sub folders.
The scripts exist to allow an identical build as found in the distributed addon archives via AMO.

## Run the addon

Requires built wasm binaries.

Choose "debug addon" from either Firefox or Thunderbird and
aim the file chooser towards the corresponding `manifest.json` file.

Maybe the addon is also available in the addon stores.. have a look ;)
