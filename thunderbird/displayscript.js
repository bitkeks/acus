function ds() {
    let textcontent_elem = null;
    let textcontent = "";
    for (child of document.body.children) {
        if (child.classList.contains("moz-text-flowed") || child.classList.contains("moz-text-plain")) {
            textcontent_elem = child;
            textcontent = child.textContent;
            break
        }
    }

    if (textcontent_elem === null) {
        return;
    }

    let before_lines = [];
    let after_lines = [];
    let cipher_lines = [];
    let cipher_lines_active = false;
    let is_before = true;

    for (line of textcontent.split("\n")) {
        if (line === "-----END AGE ENCRYPTED FILE-----") {
            cipher_lines_active = false;
            cipher_lines.push(line);
            is_before = false;
            continue;
        }

        if (line === "-----BEGIN AGE ENCRYPTED FILE-----") {
            cipher_lines_active = true;
            cipher_lines.push(line);
            continue;
        }

        if (cipher_lines_active) {
            cipher_lines.push(line);
        } else if (is_before) {
            before_lines.push(line);
        } else {
            after_lines.push(line);
        }
    }

    // Use messaging to communicate with the background script to have access to wasm
    let decryption_promise = browser.runtime.sendMessage({
        command: "decrypt",
        ciphertext: cipher_lines.join("\n")
    });

    decryption_promise.then(value => {
        let html = before_lines.join("<br>\n") +
            "<br>\n" + "~~~ decrypted block start ~~~ <br>\n" +
            value.replaceAll("\n", "<br>\n") +
            "<br>\n" + "~~~ decrypted block end ~~~ <br>\n" +
            after_lines.join("<br>\n");
        textcontent_elem.innerHTML = html
    }, reason => {
        // error
        console.log("Error in decryption_promise: ", reason);
    });
}

ds();