window.addEventListener("load", initiate);

const stKey_keys = "userLocalKeys";
const stG = browser.storage.local.get;

messenger.messageDisplayScripts.register({
	js: [{
		file: "displayscript.js"
	}]
});

async function getAllStorageItems() {
    return browser.storage.local.get(stKey_keys);
}

async function getLocalKeys() {
	keys = await browser.storage.local.get(stKey_keys);
	return keys.userLocalKeys;
}

async function handleBrowserAction(tab, info) {
    messenger.tabs.create({
        url: "acus_tab/tab.html",
        active: true
    });
}

function initiate() {
    const go = new Go();
    WebAssembly.instantiateStreaming(fetch("/wasm/acus.wasm"), go.importObject).then((result) => {
        go.run(result.instance);
    }).catch((err) => {
        console.error(err);
    });

	// Testing a gzip compressed artifact. Does not work yet, we have to wait for
	// implementation of the Compression Stream API
	// https://developer.mozilla.org/en-US/docs/Web/API/Compression_Streams_API
    //~ fetch('/wasm/acus.wasm.gz')

    // Check setup of local storage
    getAllStorageItems().then((result) => {
		// Local keys
		if (result.userLocalKeys == null) {
			console.log("Set up new empty userLocalKeys array");
			browser.storage.local.set({
				userLocalKeys: []
			}).catch(err => console.log(err))
		}
	}).catch(err => console.log(err));
}

// Source: https://developer.thunderbird.net/add-ons/mailextensions/hello-world-add-on/using-content-scripts
messenger.runtime.onMessage.addListener(async (message, sender, sendResponse) => {
    if (message && message.hasOwnProperty("command")) {
        switch (message.command) {
            case "decrypt":
				let keys = await getLocalKeys();
				let cleartext = window.acusDecryptText({
					ciphertext: message.ciphertext,
					privateKey: keys[0]
				});
				return cleartext.cleartext;

            case "encrypt":
                let ciphertext = window.acusEncryptText({
                    cleartext: message.plaintext,
                    recipients: [message.recipient_pubkey]
                });
                return ciphertext;

            case "generate":
                return window.acusGenerateKey();
        }
    }
});

messenger.browserAction.onClicked.addListener(handleBrowserAction);
