const mb = document.querySelector("#messagebox");
const gkb = document.querySelector("#generate_key_box");
const cr = document.getElementById("ciphertext_result");

function loaded() {
    gkb.style.display = "none";
    cr.style.display = "none";

    mb.innerText = "";
    let ulk = messenger.storage.local.get('userLocalKeys');
    ulk.then(result => {
        if (result.userLocalKeys.length == 0) {
            mb.innerText = "No key present yet.";
        } else {
            document.querySelector("#acuskey > #key").value = result.userLocalKeys[0];
            mb.innerText = "Private secret key loaded from storage.";
        }
    });
}

function saveKey(e) {
    let key = document.querySelector("#acuskey > #key").value;

    if (key.length != 74 || !key.startsWith("AGE-SECRET-KEY-")) {
        mb.innerText = "Something was wrong with your new key. Please check if it is a valid secret key!";
        return;
    }

    messenger.storage.local.set({
        userLocalKeys: [key]
    }).then(res => {
        console.log("New key saved successfully");
        mb.innerText = "New key saved successfully";
    }).catch(err => console.log(err))
}

function generateKey(e) {
    browser.runtime.sendMessage({
        command: "generate"
    }).then(keypair => {
        gkb.style.display = "";
        document.getElementById("generated_public").textContent = keypair.pubkey;
        document.getElementById("generated_secret").textContent = keypair.secret;
    }).catch(err => console.log(err));
}

async function encrypt(e) {
    let plaintext = document.querySelector("textarea#plaintext").value;
    let recipient_pubkey = document.querySelector("input#recipient_key").value;

    messenger.runtime.sendMessage({
        command: "encrypt",
        plaintext: plaintext,
        recipient_pubkey: recipient_pubkey
    }).then(ciphertext => {
        console.log(ciphertext);
        cr.style.display = "";

        if (ciphertext == null) {
            cr.textContent = "There was an error encrypting your text. Is the recipient key correct?";
            return;
        }
        cr.textContent = ciphertext;
    }).catch(err => console.log(err));
}

document.addEventListener('DOMContentLoaded', loaded);
document.querySelector("#acuskey").addEventListener("submit", saveKey);
document.querySelector("#encrypt").addEventListener("submit", encrypt);
document.querySelector("#generate_button").addEventListener("click", generateKey);
