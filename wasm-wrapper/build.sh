#!/bin/sh

export GOOS=js 
export GOARCH=wasm 
ROOTDIR=$(dirname "$(readlink -f "$0")")

cd "$ROOTDIR" || exit
echo "Working in $(pwd)"

echo "Building with Go"
go build -ldflags="-s -w" -v -o ./build/acus.wasm acus.go || exit

echo "Copying all wasm binaries to webext"
cp -av ./build/*.wasm $ROOTDIR/../thunderbird/wasm/
cp -av ./build/*.wasm $ROOTDIR/../firefox/wasm/
