#!/bin/bash

full_path=$(realpath $0)
PROJ=$(dirname $full_path)

rm -v $PROJ/firefox/wasm/acus.wasm
rm -v $PROJ/thunderbird/wasm/acus.wasm
rm -v $PROJ/wasm-wrapper/build/acus.wasm

podman build -t acus-builder-f35 -f wasm-wrapper/Containerfile
podman run -ti -v $(pwd):/data:rw,Z acus-builder-f35 bash /data/wasm-wrapper/build.sh
