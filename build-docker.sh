#!/bin/bash

docker build -t acus-builder-f35 -f wasm-wrapper/Containerfile
docker run -ti -v $(pwd):/data:rw,Z acus-builder-f35 bash /data/wasm-wrapper/build.sh
