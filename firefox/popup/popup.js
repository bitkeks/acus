function onload() {
	let key_input_field = document.querySelector("input[type=text]");
	browser.storage.local.get().then((obj, err) => {
		key_input_field.value = obj.agekey;
	});
}

window.addEventListener("load", onload);

let keyform = document.querySelector("form#key");

keyform.addEventListener("submit", e => {
	e.preventDefault();
	let key_field = e.target.querySelector("input[type=text]");

	browser.storage.local.set({
		agekey: key_field.value
	}).then((ok, err) => {
		//~ console.log(ok, err);
	});
});

