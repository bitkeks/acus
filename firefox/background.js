function initiate() {
    const go = new Go();
    WebAssembly.instantiateStreaming(fetch("/wasm/acus.wasm"), go.importObject).then((result) => {
        go.run(result.instance);
    }).catch((err) => {
        console.error(err);
    });
}

// Source: https://gist.github.com/Deliaz/e89e9a014fea1ec47657d1aac3baa83c
function arrayBufferToBase64(buffer) {
    let binary = '';
    let bytes = new Uint8Array(buffer);
    let len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
}

window.addEventListener("load", initiate);

browser.runtime.onMessage.addListener(async (message, sender, sendResponse) => {
    if (message && message.hasOwnProperty("command")) {
        switch (message.command) {
            case "encrypt":
				let b64_clearbytes = arrayBufferToBase64(message.clearbytes);

				let ciphertext = window.acusEncryptBytes({
					clearbytes: b64_clearbytes,
					recipients: [message.recipient]
				});
				return ciphertext;
        }
    }
});
