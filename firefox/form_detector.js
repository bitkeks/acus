function encrypt_upload(event) {
	event.preventDefault();
	console.log(event);
}

const acuskey_forms = document.querySelectorAll("[data-acuskey]");

// In combination with provided test setup
let af = acuskey_forms[0];
af.querySelector("#encryption-status-infobox").innerText = "Acus found, uploads will be encrypted!";
af.querySelector("#encryption-status-infobox").style.color = "green";

af.addEventListener("submit", async e => {
	e.preventDefault();
	let acuskey = e.originalTarget.dataset.acuskey;
	let fileinput = e.originalTarget.querySelector("input[type=file]");
	let file = fileinput.files[0];

	console.log("Sending base64 encoded file arrayBuffer to background script..");

	let ciphertext = await browser.runtime.sendMessage({
		command: "encrypt",
		clearbytes: await file.arrayBuffer(),
		recipient: acuskey
	});

	let cipherblob = new Blob([ciphertext], { type: "text/plain" });
	let filename = e.originalTarget[0].files[0].name + ".acus";
	let cipherfile = new File([cipherblob], filename, { type: "text/plain" });

	let fd = new FormData();
	fd.append("file", cipherfile);

	let resp = await fetch(e.originalTarget.action, {
		body: fd,
		headers: {
			// Do not set Content-Type! Browser must figure out boundary
			//~ "Content-Type": e.originalTarget.enctype
		},
		method: e.originalTarget.method,
	});

	if (resp.ok) {
		window.location.reload();
	} else {
		console.log(resp);
	}
});

